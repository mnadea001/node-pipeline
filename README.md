```
npm install
npm run test 
git rm -r --cached node_modules
```


Dans le gitlab-ci:
- les artefacts permettent de "transférer" des variables d'un conteneur vers un autre
- Pour les tests postman, on ajoute un entrypoint pour "écraser" le comportement par défaut car normalement postman exécute directement les tests. Ici on souhaite démarrer l'application en meme temps que les tests, d'ou l'ajout du entrypoint


## Postman 

```
newman --version
newman run ./tests/TP-J5.postman_collection.json 

newman run ./tests/TP-J5-v2.postman_collection.json --env-var "URL_APP=http://localhost:3000"

```